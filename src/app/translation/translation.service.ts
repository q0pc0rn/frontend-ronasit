import { TranslationApiResponse } from './translation-api-response';
import { TranslationApiRequest } from './translation-api-request';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  private key = 'AIzaSyBPIsl7ydUMgF9GkSlZodcPZOZmUx5xHew';
  
  constructor(private http: HttpClient) { }

  public translateText(text: string): Observable<any> {
    return this.http.get<any>('https://translation.googleapis.com/language/translate/v2', {
      params: this.buildHttpParams({
        q: text,
        source: 'en',
        target: 'ru',
        format: 'text'
      })
    })
    .pipe(
      map(({ data }: any) => data.translations[0].translatedText)
    )
  }

  private buildHttpParams(query: any): HttpParams {
    return new HttpParams({
      fromObject: { ...query, key: this.key }
    });
  }
}
