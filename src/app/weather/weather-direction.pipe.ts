import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'weatherDirection'
})
export class WeatherDirectionPipe implements PipeTransform {

  transform(value: number): string {
    if (value > 22.3 && value < 67.3) {
      return "северо-западный";
    }

    if (value > 67.3 && value < 112.3) {
      return "восточный";
    }

    if (value > 112.3 && value < 157.3) {
      return "юго-восточный";
    }

    if (value > 157.3 && value < 202.3) {
      return "южный";
    }

    if (value > 202.3 && value < 247.3) {
      return "юго-западный";
    }

    if (value > 247.3 && value < 292.3) {
      return "западный";
    }

    if (value > 292.3 && value < 337.3) {
      return "северо-западный";
    }

    return "северный";
  }
}
