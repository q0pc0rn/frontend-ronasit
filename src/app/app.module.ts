import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { WeatherDirectionPipe } from './weather/weather-direction.pipe';
import { WeatherService } from './weather/weather.service';
import { TemperatureUSEUPipe } from './weather/temperature-us-eu.pipe';
import { WeatherComponent } from './weather/weather.component';
import { WeatherCloudsToIconPipe } from './weather/weather-clouds-to-icon.pipe';
import { TranslatePipe } from './translation/translate.pipe';
import { WeatherPressurePipe } from './weather/weather-pressure.pipe';

@NgModule({
  declarations: [
    AppComponent,
    WeatherDirectionPipe,
    TemperatureUSEUPipe,
    WeatherComponent,
    WeatherCloudsToIconPipe,
    TranslatePipe,
    WeatherPressurePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
