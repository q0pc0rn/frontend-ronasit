export interface GeolocationApiResponse {
  results: {
    [index: number]: { address_component: AddressComponent[]}
  }
}

export interface AddressComponent {
  long_name: string,
  short_name: string,
  types: string[]
;}