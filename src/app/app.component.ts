import { GeolocationService } from './geolocation/geolocation.service';
import { IGeolocation } from './geolocation/geolocation.interface';
import { FormGroup, FormControl } from '@angular/forms';
import { Component , OnInit, OnDestroy, ViewChild } from '@angular/core';
import { WeatherService } from './weather/weather.service';
import { Observable, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { WeatherApiResponse } from './weather/weather-api-response';
import {} from "googlemaps";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnDestroy, OnInit {
  public isEU: boolean = true;
  isShow: boolean = false;
  city: string;
  geolocationData: IGeolocation;
  map: google.maps.Map;
  
  form = new FormGroup({
    q: new FormControl('')
  });

  private formQuery$: Subscription;

  weatherData: Observable<WeatherApiResponse>;

  constructor(private weatherService: WeatherService, private geolocationService: GeolocationService) {
    // if (!this.form.value.q) {
    //   this.formQuery$ = this.form.valueChanges.pipe(debounceTime(500)).subscribe(
    //     value => this.weatherData = this.weatherService.getWeather(value)
    //   );
    // }
  }

  onSubmit(): void {
    this.weatherData = this.weatherService
    .getWeather(this.form.value);
    this.isShow = false;
  }

  ngOnDestroy(): void {
    this.formQuery$.unsubscribe();
  } 

  ngOnInit(){
    this.init()
    .then(console.log)
    .catch(console.error);
  }

  async init(): Promise<any> {
    const {latitude, longitude} = await this.getCurrentPossition();
    this.geolocationService.getCityByCoord(latitude,longitude)
    .subscribe(response => {
      this.form.patchValue({q: response});
      this.weatherData = this.weatherService.getWeather({q: response});//$
    }, console.error);
  }

  getCurrentPossition(): Promise<{latitude: number, longitude: number}> {
    return new Promise((resolve, reject) => {
      if (!window.navigator.geolocation) {
        return null;
      }
      window.navigator.geolocation.getCurrentPosition(
        position => resolve(position.coords),
        error => reject(error)
      );
    });
  }

  findWeatherOfCity(){
    this.init()
    .then()
    .catch(console.error);
  }
}
