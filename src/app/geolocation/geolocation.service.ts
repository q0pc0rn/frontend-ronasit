import { GeolocationApiResponse } from './geolocation-api-response';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GeolocationService {

  private key = 'AIzaSyBPIsl7ydUMgF9GkSlZodcPZOZmUx5xHew';

  constructor(private http: HttpClient) { }

  public getCityByCoord(lat: number, lon: number): Observable<string> {
    return this.http.get<GeolocationApiResponse>('https://maps.googleapis.com/maps/api/geocode/json', {
      params: this.buildHttpParams({
        latlng: `${lat},${lon}`
      })
    }).pipe(
      map(({ results }: any) => results[0].address_components.find(x => x.types.includes('locality')).long_name)
    )
  }

  private buildHttpParams(query: any): HttpParams {
    return new HttpParams({
      fromObject: { ...query, key: this.key }
    });
  }
}
