import { TranslationService } from './../translation/translation.service';
import { Component, OnInit, Input } from '@angular/core';
import { WeatherApiResponse } from './weather-api-response';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.sass']
})
export class WeatherComponent implements OnInit {

  @Input() weatherData: WeatherApiResponse;
  @Input() isEU: boolean;

  private _description: string;

  constructor(private translationService: TranslationService) {
    // this._description = this.weatherData.weather[0].description;
   }

  ngOnInit(){

  }


}
