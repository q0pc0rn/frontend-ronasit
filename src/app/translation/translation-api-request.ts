export interface TranslationApiRequest {
  q: string,
  source: string
  target: string,
  format: string,
  key: string
}
