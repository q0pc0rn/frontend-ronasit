export interface WeatherApiRequest {
    q: string,
    appid?: string
}
