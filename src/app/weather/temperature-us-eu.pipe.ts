import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'temperatureUSEU'
})
export class TemperatureUSEUPipe implements PipeTransform {

  transform(value: any, isEU = true): any {
    if (isEU) {
      return Math.round(value - 273.15);
    }
    return Math.round((value - 273.15) * 1.8 + 32);
  }

}
