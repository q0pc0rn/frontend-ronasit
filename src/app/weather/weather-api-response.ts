export interface WeatherApiResponse extends Object {
    weather: {
        [index: number]: {
            id: number;
            description: string;
        }
    };
    wind: {
        speed: number;
        deg: number;
    };
    main: {
        temp: number;
        pressure: number;
        humidity: number;
        temp_min: number;
        temp_max: number;
    };
    rain: {
        '1h': number;
    }
    clouds:{
        all: number;
        };
}
