import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'weatherCloudsToIcon'
})
export class WeatherCloudsToIconPipe implements PipeTransform {

  transform(weatherId: number): string {
    if (!weatherId) return '';
    const _weatherId: string = weatherId.toString();
    
    switch (true) {
      case (_weatherId.startsWith('801')): {
        return '/assets/weather-icons/partly cloudy.svg'
      }
      case (_weatherId.startsWith('800')): {
        return '/assets/weather-icons/sun.svg'
      }
      case (_weatherId.startsWith('80') || _weatherId.startsWith('7')): {
        return '/assets/weather-icons/cloud.svg'
      }
      case (_weatherId.startsWith('5') || _weatherId.startsWith('3') || _weatherId.startsWith('6')): {
        return '/assets/weather-icons/rain.svg'
      }
      case (_weatherId.startsWith('2')): {
        return '/assets/weather-icons/strom.svg'
      }

    }


    return null;
  }

}
