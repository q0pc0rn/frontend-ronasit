export interface TranslationApiResponse {
  data: {
    translations: {
      translatedText: string
    }[]
  }
}
