import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'weatherPressure'
})
export class WeatherPressurePipe implements PipeTransform {

  transform(value: number, args?: any): number {
    return  Math.round(value * 0.750063755419211);
  }

}
