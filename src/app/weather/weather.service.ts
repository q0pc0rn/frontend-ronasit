import { HttpParams, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WeatherApiRequest } from './weather-api-request';
import { WeatherApiResponse } from './weather-api-response';
import { IGeolocation } from '../geolocation/geolocation.interface';


@Injectable()
export class WeatherService {

  private appid = 'c0c935816b1822213b71a6dd6d755700';

  constructor(private http: HttpClient) { }

  public getWeather(query: WeatherApiRequest): Observable<WeatherApiResponse> {
    return this.http.get<WeatherApiResponse>('http://api.openweathermap.org/data/2.5/weather', {
      params: this.buildHttpParams(query)
    });
  }
  
  public getWeatherGeo(query: IGeolocation): Observable<WeatherApiResponse> {
    return this.http.get<WeatherApiResponse>('http://api.openweathermap.org/data/2.5/weather', {
      params: this.buildHttpParams(query)
    });
  }

  private buildHttpParams(queryString: any): HttpParams {
    return new HttpParams({
      fromObject: {...queryString, appid: this.appid}
    });
  }
}
